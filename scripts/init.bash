#!/usr/bin/env bash

log() { echo "$(date) $(basename $0): $*"; }
die() { log "$*" >&2; exit 1; }

# load constants
SCRIPTSDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/"
CONSTANTS="${SCRIPTSDIR}constants"
[ -f ${CONSTANTS} ] || die "missing ${CONSTANTS}"
source ${CONSTANTS}

# ensure nwjs 
if [ ! -f "${SCRIPTSDIR}${NWJS}" ]; then
  [ -d  "${SCRIPTSDIR}../nwjs" ] || mkdir "${SCRIPTSDIR}../nwjs"
  cd "${SCRIPTSDIR}../nwjs"
  if [ ! -f "${ZIP}" ]; then
      OLDZIP=$(find ~/ -name "${ZIP}")
      if [ -f "${OLDZIP}" ]; then
        cp "${OLDZIP}" .
      else
        wget "${URL}" > /dev/null || exit $?
      fi      
  fi
  unzip ${ZIP} > /dev/null || exit $? 
  cd "${SCRIPTSDIR}"
fi
[ -f "${SCRIPTSDIR}${NWJS}" ] || die "failed with ${NWJS}"

# ensure src
[ -d "${SCRIPTSDIR}${SRCDIR}" ] || mkdir "${SCRIPTSDIR}${SRCDIR}"
if [ ! -f "${SCRIPTSDIR}${INDEXHTML}" ]; then
cat > "${SCRIPTSDIR}${INDEXHTML}" << EOF
<!DOCTYPE html>
<html>
<head>
  <title>${TITLE}</title>
</head>
<body style="width: 100%; height: 100%;">
  <script src="./main.js"></script>
</body>
</html>  
EOF
fi

if [ ! -f "${SCRIPTSDIR}${MAINJS}" ]; then
cat > "${SCRIPTSDIR}${MAINJS}" << EOF
import { app } from './app'
const { watch } = require('fs');
const reloadWatcher = watch('./main.js', function() {
  location.reload()
  reloadWatcher.close()
})
document.body.insertBefore(app(),document.body.childNodes[0])
EOF
fi

if [ ! -f "${SCRIPTSDIR}${MAKEFILE}" ]; then
cat > "${SCRIPTSDIR}${MAKEFILE}" << EOF
#!make
MAKEFLAGS += --silent
#include .env
#export \$(shell sed 's/=.*//' .env)
dev:
  cd src && rollup -f es main.mjs -o main.js -w 
build:
  echo 'zip release'
.PHONY: dev
.PHONY: watch
EOF
fi


